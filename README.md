Petrelli et al., 2016 - Quaternary Geochronology
twitter: @MauPetrelli

A) Python environment
If you are not an expert, we suggest installing the the Anaconda Python distribution as follow.
1) download Anaconda Python https://www.continuum.io/downloads
2) follow the instruction on the website
Note that we worked on the Python 2.7 version

B) Machine Learning environment
We utilized the Scikit-learn environment: http://scikit-learn.org
if you are utilizing the Anaconda Python the Scikit-learn installation is simple:
1) type conda install scikit-learn
2) type conda --v to check the installed version

C) Closure Effect investigation
We utilized the Scikit-bio environment: http://scikit-bio.org
if you are utilizing the Anaconda Python the Scikit-bio installation is simple:
1) conda install -c https://conda.anaconda.org/biocore scikit-bio

C) Replicate the study by Petrelli et al., 2016
Run the .py scripts in sequence (they are numbered from 1 to 8)