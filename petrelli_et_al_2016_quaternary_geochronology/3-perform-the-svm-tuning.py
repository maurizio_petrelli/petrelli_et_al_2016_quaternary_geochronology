# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings("ignore")

import numpy as np
from sklearn.svm import SVC
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.grid_search import GridSearchCV
from sklearn.preprocessing import StandardScaler


C_range = np.logspace(-2, 10, 13)
gamma_range = np.logspace(-8, 4, 13)


param_grid = dict(gamma=gamma_range, C=C_range)
cv = StratifiedShuffleSplit(y17Dmt, n_iter=5, test_size=0.3, random_state=42)
grid = GridSearchCV(SVC(), param_grid=param_grid, cv=cv)
grid.fit(X17Dmt, y17Dmt.ravel())

print("The best parameters are %s with a score of %0.2f"
      % (grid.best_params_, grid.best_score_))