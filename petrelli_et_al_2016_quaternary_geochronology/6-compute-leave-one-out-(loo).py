# -*- coding: utf-8 -*-

from sklearn.svm import SVC
import numpy as np


# split the learning dataset (X_train) with relative labels (y_train) from the unknowns (UNKN)
UNKNidx = np.array([],int)


for idx in range(len(X17Dmt)):
    if y17Dmt[idx]==0:
        UNKNidx = np.append(UNKNidx, idx)


X17Dmt_new = np.delete(X17Dmt,UNKNidx, axis=0)
y17Dmt_new = np.delete(y17Dmt,UNKNidx, axis=0)

SVM =    SVC(gamma=10**-1, C=100)

res = np.zeros(len(y17Dmt_new))


for idx in range(len(y17Dmt_new)):

    X_train = np.delete(X17Dmt_new, (idx), axis=0)
    X_tofit =  X17Dmt_new[idx]     
    y_train= np.delete(y17Dmt_new, idx)      
    
    SVM.fit(X_train, y_train.ravel())
    
    res[idx] = SVM.predict(X_tofit.reshape(1, -1))  
    
    print idx, ") ", res[idx], "  ", y17Dmt_new[idx]
    
