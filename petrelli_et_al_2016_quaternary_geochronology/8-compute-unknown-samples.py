# -*- coding: utf-8 -*-

from sklearn.svm import SVC
import numpy as np


n = 0


# stetup te support vector machine (gamma and C were perviously defined by the tuning procedure)
SVM =    SVC(gamma=10**-1, C=100)


# split the learning dataset (X_train) with relative labels (y_train) from the unknowns (UNKN)
UNKNidx = np.array([],int)
trainidx = np.array([],int)
y_train = np.array([],int)

for idx in range(len(X17Dmt)):
    if y17Dmt[idx]==0:
        UNKNidx = np.append(UNKNidx, idx)
    else:
        trainidx = np.append(trainidx, idx)
        y_train = np.append(y_train, y17Dmt[idx])

UNKN = np.delete(X17Dmt,trainidx, axis=0)
X_train = np.delete(X17Dmt,UNKNidx, axis=0)


# Now learn the system       
SVM.fit(X_train, y_train)

res = np.zeros(len(UNKN))

# Finally, analyze the unknowns
for idx in range(len(UNKN)):
    X_tofit = UNKN[idx]  
    res[idx] = SVM.predict(X_tofit.reshape(1, -1))  
    print idx, ") ", res[idx]
    
