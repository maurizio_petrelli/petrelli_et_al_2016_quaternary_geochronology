# -*- coding: utf-8 -*-

import matplotlib.pylab as plt
import numpy as np
from sklearn import  metrics



#some simple statistics

n01 = 0
n02 = 0
n03 = 0
n04 = 0
n05 = 0
n06 = 0
n07 = 0
n08 = 0
n09 = 0
n10 = 0

tot01 = 0
tot02 = 0
tot03 = 0
tot04 = 0
tot05 = 0
tot06 = 0
tot07 = 0
tot08 = 0
tot09 = 0
tot10 = 0



Res01 = np.array([],int)
Res02 = np.array([],int)
Res03 = np.array([],int)
Res04 = np.array([],int)
Res05 = np.array([],int)
Res06 = np.array([],int)
Res07 = np.array([],int)
Res08 = np.array([],int)
Res09 = np.array([],int)
Res10 = np.array([],int)

for idx in range(len(y17Dmt_new)):
    if y17Dmt_new[idx]==1:
        tot01 = tot01 + 1
        if y17Dmt_new[idx]==res[idx]:
            n01=n01+1
            Res01 = np.append(Res01, res[idx])
        else:
            #print idx
            Res01 = np.append(Res01, res[idx])
    if y17Dmt_new[idx]==2:
        tot02 = tot02 + 1
        if y17Dmt_new[idx]==res[idx]:
            n02=n02+1
            Res02 = np.append(Res02, res[idx])
        else:
            #print idx
            Res02 = np.append(Res02, res[idx])
    if y17Dmt_new[idx]==3:
        tot03 = tot03 + 1
        if y17Dmt_new[idx]==res[idx]:
            n03=n03+1
            Res03 = np.append(Res03, res[idx])
        else:
            #print idx
            Res03 = np.append(Res03, res[idx])
    if y17Dmt_new[idx]==4:
        tot04 = tot04 + 1
        if y17Dmt_new[idx]==res[idx]:
            n04=n04+1
            Res04 = np.append(Res04, res[idx])
        else:
            #print idx
            Res04 = np.append(Res04, res[idx])
    if y17Dmt_new[idx]==5:
        tot05 = tot05 + 1
        if y17Dmt_new[idx]==res[idx]:
            n05=n05+1
            Res05 = np.append(Res05, res[idx])
        else:
            #print idx
            Res05 = np.append(Res05, res[idx])
    if y17Dmt_new[idx]==6:
        tot06 = tot06 + 1
        if y17Dmt_new[idx]==res[idx]:
            n06=n06+1
            Res06 = np.append(Res06, res[idx])
        else:
            #print idx
            Res06 = np.append(Res06, res[idx])
    if y17Dmt_new[idx]==7:
        tot07 = tot07 + 1
        if y17Dmt_new[idx]==res[idx]:
            n07=n07+1
            Res07 = np.append(Res07, res[idx])
        else:
            #print idx
            Res07 = np.append(Res07, res[idx])
    if y17Dmt_new[idx]==8:
        tot08 = tot08 + 1
        if y17Dmt_new[idx]==res[idx]:
            n08=n08+1
            Res08 = np.append(Res08, res[idx])
        else:
            #print idx
            Res08 = np.append(Res08, res[idx])  
    if y17Dmt_new[idx]==9:
        tot09 = tot09 + 1
        if y17Dmt_new[idx]==res[idx]:
            n09=n09+1
            Res09 = np.append(Res09, res[idx])
        else:
            #print idx
            Res09 = np.append(Res09, res[idx])
    if y17Dmt_new[idx]==10:
        tot10 = tot10 + 1
        if y17Dmt_new[idx]==res[idx]:
            n10=n10+1
            Res10 = np.append(Res10, res[idx])
        else:
            #print idx
            Res10 = np.append(Res10, res[idx])


percent01 = 100 * n01/tot01
percent02 = 100 * n02/tot02
percent03 = 100 * n03/tot03
percent04 = 100 * n04/tot04
percent05 = 100 * n05/tot05
percent06 = 100 * n06/tot06
percent07 = 100 * n07/tot07
percent08 = 100 * n08/tot08
percent09 = 100 * n09/tot09
percent10 = 100 * n10/tot10



tot = tot01+tot02+tot03+tot04+tot05+tot06+tot07+tot08+tot09+tot10
ttotn = n01+n02+n03+n04+n05+n06+n07+n08+n09+n10

totPercent = 100 * ttotn / tot

print "TOT= ", tot
print "TOT%= ", totPercent

print "percent01= ", percent01
print "percent02= ", percent02
print "percent03= ", percent03
print "percent04= ", percent04
print "percent05= ", percent05
print "percent06= ", percent06
print "percent07= ", percent07
print "percent08= ", percent08
print "percent09= ", percent09
print "percent10= ", percent10



# Confusion Matrix


conf_arr = metrics.confusion_matrix(y17Dmt_new,res)
norm_conf = []
for i in conf_arr:
    a = 0
    tmp_arr = []
    a = sum(i, 0)
    for j in i:
        tmp_arr.append(float(j)/float(a))
    norm_conf.append(tmp_arr)

fig = plt.figure()
plt.clf()
ax = fig.add_subplot(111)
ax.set_aspect(1)
res1 = ax.imshow(np.array(norm_conf), cmap=plt.cm.hot, 
                interpolation='nearest')

width = len(norm_conf)
height = len(norm_conf[0])

for x in xrange(width):
    for y in xrange(height):
        if x==y:
            if (norm_conf[x][y]>0.005):
                ax.annotate(str(round(100*norm_conf[x][y],1)), xy=(y, x), color='black', horizontalalignment='center',verticalalignment='center')
            elif (norm_conf[x][y]==0):
                ax.annotate("0", xy=(y, x), color='black', horizontalalignment='center',verticalalignment='center')    
            else:
                ax.annotate("<0.5", xy=(y, x), color='black', horizontalalignment='center',verticalalignment='center')
           
        else:
            if (norm_conf[x][y]>0.005):
                ax.annotate(str(round(100*norm_conf[x][y],1)), xy=(y, x), color='white', horizontalalignment='center',verticalalignment='center')
            elif (norm_conf[x][y]==0):
                ax.annotate("0", xy=(y, x), color='white', horizontalalignment='center',verticalalignment='center')    
            else:
                ax.annotate("<0.5", xy=(y, x), color='white', horizontalalignment='center',verticalalignment='center')
             
cb = fig.colorbar(res1)
labels = ['VV','EV','PF','AI','RMP','TMP','PI','IAVP','MV','ERP']
plt.xticks(range(width), labels[:width])
plt.yticks(range(height), labels[:height])



