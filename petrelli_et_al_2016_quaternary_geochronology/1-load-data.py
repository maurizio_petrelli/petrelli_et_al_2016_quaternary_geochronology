# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings("ignore")

import pandas as pd



# Import the Dataset from GEOROCK
IT_VOLCANIC = pd.read_csv('georock-data.csv')


# Only volcanic rocks
IT_VOLCANIC=IT_VOLCANIC[IT_VOLCANIC['ROCK TYPE'].str.contains("VOLCANIC", na=False) == True]

# Samples marked as altered are rejected here
IT_VOLCANIC=IT_VOLCANIC[IT_VOLCANIC.ALTERATION.str.contains("ALTERED", na=False) == False]

# we extract form the dataset three sub populations: 'volcanic glasses', 'melt inclusions' ans 'whole rocks'
IT_VOLCANIC_GLASS =IT_VOLCANIC[IT_VOLCANIC['TYPE OF MATERIAL'].str.contains("VOLCANIC GLASS", na=False) == True]
IT_VOLCANIC_WR =IT_VOLCANIC[IT_VOLCANIC['TYPE OF MATERIAL'].str.contains("WHOLE ROCK", na=False) == True]
IT_VOLCANIC_INCLUSIONS =IT_VOLCANIC[IT_VOLCANIC['TYPE OF MATERIAL'].str.contains("INCLUSION", na=False) == True]


# Now we adjust the labeling of  columns names
dataframes = [IT_VOLCANIC_GLASS,IT_VOLCANIC_WR,IT_VOLCANIC_INCLUSIONS]

for df in dataframes:
    df.columns = [c.replace('(', '') for c in df.columns]
    df.columns = [c.replace(')', '') for c in df.columns]
    df.columns = [c.replace('%', '') for c in df.columns]
    df.columns = [c.replace(' ', '') for c in df.columns]
    df.columns = [c.replace('.', '') for c in df.columns]   
    df['Latitude']= (df.LATITUDEMAX+df.LATITUDEMIN)/2
    df['Longitude']= (df.LONGITUDEMAX+df.LONGITUDEMIN)/2



    
for df in dataframes:    
    df.columns = [c.replace('WT', '') for c in df.columns]
    df.columns = [c.replace('PPM', '') for c in df.columns]
    df.columns =[c.replace('SR', 'Sr') for c in df.columns]
    df.columns =[c.replace('TH', 'Th') for c in df.columns]
    df.columns =[c.replace('NB', 'Nb') for c in df.columns]
    df.columns =[c.replace('TA', 'Ta') for c in df.columns]
    df.columns =[c.replace('LA', 'La') for c in df.columns]
    df.columns =[c.replace('CE', 'Ce') for c in df.columns]
    df.columns =[c.replace('ND', 'Nd') for c in df.columns]
    df.columns =[c.replace('ZR', 'Zr') for c in df.columns]
    df.columns =[c.replace('HF', 'Hf') for c in df.columns]
    df.columns =[c.replace('SM', 'Sm') for c in df.columns]
    df.columns =[c.replace('GD', 'Gd') for c in df.columns]
    df.columns =[c.replace('YB', 'Yb') for c in df.columns]
    df.columns =[c.replace('LU', 'Lu') for c in df.columns]
    df.columns =[c.replace('TI', 'Ti') for c in df.columns]
    df.columns =[c.replace('RB', 'Rb') for c in df.columns]
    df.columns =[c.replace('BA', 'Ba') for c in df.columns]    
    df.columns =[c.replace('TiO2', 'TIO2') for c in df.columns]
    df.columns =[c.replace('SiO2', 'SIO2') for c in df.columns]
    df.columns =[c.replace('Al2O3', 'AL2O3') for c in df.columns]
    df.columns =[c.replace('CaO', 'CAO') for c in df.columns]
    df.columns =[c.replace('MgO', 'MGO') for c in df.columns]
    df.columns =[c.replace('MnO', 'MNO') for c in df.columns]
    df.columns =[c.replace('Na2O', 'NA2O') for c in df.columns]
    df.columns =[c.replace('LOCATiON', 'LOCATION') for c in df.columns]
    
    


# if you'd like to limit your investgation to a sigle or two su-population do it here
dataframes1 = [IT_VOLCANIC_GLASS,IT_VOLCANIC_WR,IT_VOLCANIC_INCLUSIONS]


dataTOTF = pd.concat(dataframes1,ignore_index=True)
  
# we are going to tranform all the iron as Fe2O3tot 
FE2O3Tfinal1 = pd.DataFrame(columns=['FE2O3Tfinal'],index=dataTOTF.index)
FE2O3Tfinal3 = pd.DataFrame(columns=['FE2O3Tfinal'],index=dataTOTF.index)

FE2O3Tfinal1['FE2O3Tfinal'] = dataTOTF.FEOT/0.8998
FE2O3Tfinal3['FE2O3Tfinal'] = dataTOTF.FE2O3 + dataTOTF.FEO / 0.8998

dataTOTF.ix[dataTOTF.FE2O3T >= 0, "FE2O3Tfinal"] = dataTOTF.FE2O3T

dataTOTF.update(FE2O3Tfinal1)
dataTOTF.update(FE2O3Tfinal3)


dataTOTF['SumTracesPercent'] = (dataTOTF.Nb + dataTOTF.Zr + dataTOTF.La + dataTOTF.Ce + dataTOTF.Sr + dataTOTF.Ba + dataTOTF.Rb) / 10000

#we are goint to tranform major elements as 'water free'
CloumnList=['SIO2','TIO2','AL2O3','FE2O3Tfinal','CAO','MGO','MNO','K2O','NA2O','P2O5','SumTracesPercent']


dataTOTF['TotalWT'] = dataTOTF[CloumnList].sum(axis=1)  
dataTOTF = dataTOTF[(dataTOTF.TotalWT >= 94)&(dataTOTF.TotalWT <= 103)] 

                    
  
dataTOTF['SIO2N'] = (dataTOTF.SIO2)/dataTOTF.TotalWT
dataTOTF['TIO2N'] = (dataTOTF.TIO2)/dataTOTF.TotalWT
dataTOTF['AL2O3N'] = (dataTOTF.AL2O3)/dataTOTF.TotalWT
dataTOTF['FE2O3TN'] = (dataTOTF.FE2O3Tfinal)/dataTOTF.TotalWT
dataTOTF['CAON'] = (dataTOTF.CAO)/dataTOTF.TotalWT
dataTOTF['MGON'] = (dataTOTF.MGO)/dataTOTF.TotalWT
dataTOTF['MNON'] = (dataTOTF.MNO)/dataTOTF.TotalWT
dataTOTF['NA2ON'] = (dataTOTF.NA2O)/dataTOTF.TotalWT
dataTOTF['K2ON'] = (dataTOTF.K2O)/dataTOTF.TotalWT
dataTOTF['P2O5N'] = (dataTOTF.P2O5)/dataTOTF.TotalWT
dataTOTF['NbN'] = (dataTOTF.Nb)/( 10000 * dataTOTF.TotalWT)
dataTOTF['ZrN'] = (dataTOTF.Zr)/( 10000 * dataTOTF.TotalWT)
dataTOTF['LaN'] = (dataTOTF.La)/( 10000 * dataTOTF.TotalWT)
dataTOTF['CeN'] = (dataTOTF.Ce)/( 10000 * dataTOTF.TotalWT)
dataTOTF['SrN'] = (dataTOTF.Sr)/( 10000 * dataTOTF.TotalWT)
dataTOTF['BaN'] = (dataTOTF.Ba)/( 10000 * dataTOTF.TotalWT)
dataTOTF['RbN'] = (dataTOTF.Rb)/( 10000 * dataTOTF.TotalWT)

#useful to plot the total alkalies Vs silica diagram
dataTOTF['TotalAlkali'] = dataTOTF.K2ON + dataTOTF.NA2ON

# We are going to assing different labels for the different magmatic provinces

# 1 VESUVIUS VOLCANO (VV)
VESUVIUS =dataTOTF[dataTOTF['LOCATION'].str.contains("VESUVIUS", na=False) == True]
VESUVIUS['controlcode'] = 1


# 2 ETNA VOLCANO (EV)
ETNA =dataTOTF[dataTOTF['LOCATION'].str.contains("ETNA", na=False) == True]
ETNA['controlcode'] = 2

# 3 PHLEGREAN FIELDS (PF)
ISCHIA =dataTOTF[dataTOTF['LOCATION'].str.contains("ISCHIA", na=False) == True]
FLEGREI =dataTOTF[dataTOTF['LOCATION'].str.contains("FLEGREI", na=False) == True]
PROCIDA =dataTOTF[dataTOTF['LOCATION'].str.contains("PROCIDA", na=False) == True]
ISCHIA['controlcode'] = 3
FLEGREI['controlcode'] = 3
PROCIDA['controlcode'] = 3


# 4 AEOLIAN ISLANDS (AI)
AEOLIAN =dataTOTF[dataTOTF['LOCATION'].str.contains("AEOLIAN", na=False) == True]
AEOLIAN['controlcode'] = 4

# 5 ROMAN MAGMATIC PROVINCE (RMP)
LATERA = dataTOTF[dataTOTF['LOCATION'].str.contains("LATERA", na=False) == True]
MONTEFIASCONE = dataTOTF[dataTOTF['LOCATION'].str.contains("MONTEFIASCONE", na=False) == True]
BOLSENA =  dataTOTF[dataTOTF['LOCATION'].str.contains("BOLSENA", na=False) == True]
VICO =  dataTOTF[dataTOTF['LOCATION'].str.contains("VICO", na=False) == True]                  
SABATINI   = dataTOTF[dataTOTF['LOCATION'].str.contains("SABATINI", na=False) == True]             
ALBAN  = dataTOTF[dataTOTF['LOCATION'].str.contains("ALBAN", na=False) == True]                                   
LATERA['controlcode'] = 5
MONTEFIASCONE['controlcode'] = 5
BOLSENA['controlcode'] = 5
VICO['controlcode'] = 5
SABATINI['controlcode'] = 5
ALBAN['controlcode'] = 5



# 6 TUSCAN MAGMATIC PROVINCE (TMP)
VINCENZO = dataTOTF[dataTOTF['LOCATION'].str.contains("VINCENZO", na=False) == True]
ROCCASTRADA = dataTOTF[dataTOTF['LOCATION'].str.contains("ROCCASTRADA", na=False) == True]
TOLFA = dataTOTF[dataTOTF['LOCATION'].str.contains("TOLFA", na=False) == True]
CIMINO = dataTOTF[dataTOTF['LOCATION'].str.contains("CIMINO", na=False) == True]
AMIATA = dataTOTF[dataTOTF['LOCATION'].str.contains("AMIATA", na=False) == True]
GIGLIO = dataTOTF[dataTOTF['LOCATION'].str.contains("GIGLIO", na=False) == True]
CAPRAIA  = dataTOTF[dataTOTF['LOCATION'].str.contains("CAPRAIA", na=False) == True]
MONTECATINI  = dataTOTF[dataTOTF['LOCATION'].str.contains("MONTECATINI", na=False) == True]
ORCIATICO  = dataTOTF[dataTOTF['LOCATION'].str.contains("ORCIATICO", na=False) == True]
RADICOFANI  = dataTOTF[dataTOTF['LOCATION'].str.contains("RADICOFANI", na=False) == True]
ALFINA  = dataTOTF[dataTOTF['LOCATION'].str.contains("ALFINA", na=False) == True]
CECINA = dataTOTF[dataTOTF['LOCATION'].str.contains("CECINA", na=False) == True]                
VINCENZO['controlcode'] = 6
ROCCASTRADA['controlcode'] = 6
TOLFA['controlcode'] = 6
CIMINO['controlcode'] = 6
AMIATA['controlcode'] = 6
GIGLIO['controlcode'] = 6
CAPRAIA['controlcode'] = 6
MONTECATINI['controlcode'] = 6
ORCIATICO['controlcode'] = 6
RADICOFANI['controlcode'] = 6
ALFINA['controlcode'] = 6
CECINA['controlcode'] = 6



# 7 PANTELLERIA ISLLAND (PI)
PANTELLERIA =dataTOTF[dataTOTF['LOCATION'].str.contains("PANTELLERIA", na=False) == True]
PANTELLERIA['controlcode'] = 7

# 8 INTRA APPENNINE VOLCANIC PROVINCE (IAVP)
VENANZO = dataTOTF[dataTOTF['LOCATION'].str.contains("VENANZO", na=False) == True]
POLINO =dataTOTF[dataTOTF['LOCATION'].str.contains("POLINO", na=False) == True]
CUPAELLO =dataTOTF[dataTOTF['LOCATION'].str.contains("CUPAELLO", na=False) == True]
VENANZO['controlcode'] = 8
POLINO['controlcode'] = 8
CUPAELLO['controlcode'] = 8

# 9 MOUNT VULTURE (MV)
VULTURE =dataTOTF[dataTOTF['LOCATION'].str.contains("VULTURE", na=False) == True]
VULTURE['controlcode'] = 9

# 10 ERNICI ROCCAMONFINA PROVINCE (ERP)
ROCCAMONFINA =dataTOTF[dataTOTF['LOCATION'].str.contains("ROCCAMONFINA", na=False) == True]
ERNICI =dataTOTF[dataTOTF['LOCATION'].str.contains("MONTI ERNICI", na=False) == True]
ROCCAMONFINA['controlcode'] = 10
ERNICI['controlcode'] = 10

del dataTOTF

dataframes2 = [VESUVIUS,ETNA,ISCHIA,FLEGREI,PROCIDA,AEOLIAN,LATERA,MONTEFIASCONE,BOLSENA,VICO,SABATINI,ALBAN,VINCENZO,ROCCASTRADA,CIMINO,TOLFA,AMIATA,GIGLIO,CAPRAIA,MONTECATINI,ORCIATICO,RADICOFANI,ALFINA,CECINA,PANTELLERIA,VENANZO,POLINO,CUPAELLO,VULTURE,ROCCAMONFINA,ERNICI]

dataTOTF = pd.concat(dataframes2,ignore_index=True)


#if you'd like to limit the learning dataset to a particular compositional range do it here
dataTOTF = dataTOTF[(dataTOTF.SIO2N >= 0.4)&(dataTOTF.SIO2N <= 0.8)]
dataTOTF = dataTOTF[(dataTOTF.TIO2N > 0)]
dataTOTF = dataTOTF[(dataTOTF.AL2O3N > 0)]
dataTOTF = dataTOTF[(dataTOTF.FE2O3TN > 0)]
dataTOTF = dataTOTF[(dataTOTF.CAON > 0)]
dataTOTF = dataTOTF[(dataTOTF.MGON > 0)]
dataTOTF = dataTOTF[(dataTOTF.NA2ON > 0)]
dataTOTF = dataTOTF[(dataTOTF.K2ON > 0)]
dataTOTF = dataTOTF[(dataTOTF.P2O5N > 0)]
dataTOTF = dataTOTF[(dataTOTF.MNON > 0)]
dataTOTF = dataTOTF[(dataTOTF.NbN > 0)]
dataTOTF = dataTOTF[(dataTOTF.ZrN > 0)]
dataTOTF = dataTOTF[(dataTOTF.LaN > 0)]
dataTOTF = dataTOTF[(dataTOTF.CeN > 0)]
dataTOTF = dataTOTF[(dataTOTF.SrN > 0)]
dataTOTF = dataTOTF[(dataTOTF.BaN > 0)]
dataTOTF = dataTOTF[(dataTOTF.RbN > 0)]



# load unknown samples (CAIO in our case)
CAIO = pd.read_excel('Results_Caio.xlsx', sheetname='data')
CAIO['controlcode'] = 0
CAIO['SumTracesPercent'] = (CAIO.Nb + CAIO.Zr + CAIO.La + CAIO.Ce + CAIO.Sr + CAIO.Ba + CAIO.Rb) / 10000



CloumnList=['SIO2','TIO2','AL2O3','FE2O3T','CAO','MGO','MNO','K2O','NA2O','P2O5','SumTracesPercent']


CAIO['TotalWT'] = CAIO[CloumnList].sum(axis=1)  
CAIO = CAIO[(CAIO.TotalWT >= 94)&(CAIO.TotalWT <= 103)] 

  
CAIO['SIO2N'] = (CAIO.SIO2)/CAIO.TotalWT
CAIO['TIO2N'] = (CAIO.TIO2)/CAIO.TotalWT
CAIO['AL2O3N'] = (CAIO.AL2O3)/CAIO.TotalWT
CAIO['FE2O3TN'] = (CAIO.FE2O3T)/CAIO.TotalWT
CAIO['CAON'] = (CAIO.CAO)/CAIO.TotalWT
CAIO['MGON'] = (CAIO.MGO)/CAIO.TotalWT
CAIO['MNON'] = (CAIO.MNO)/CAIO.TotalWT
CAIO['NA2ON'] = (CAIO.NA2O)/CAIO.TotalWT
CAIO['K2ON'] = (CAIO.K2O)/CAIO.TotalWT
CAIO['P2O5N'] = (CAIO.P2O5)/CAIO.TotalWT
CAIO['NbN'] = (CAIO.Nb)/( 10000 * CAIO.TotalWT)
CAIO['ZrN'] = (CAIO.Zr)/( 10000 * CAIO.TotalWT)
CAIO['LaN'] = (CAIO.La)/( 10000 * CAIO.TotalWT)
CAIO['CeN'] = (CAIO.Ce)/( 10000 * CAIO.TotalWT)
CAIO['SrN'] = (CAIO.Sr)/( 10000 * CAIO.TotalWT)
CAIO['BaN'] = (CAIO.Ba)/( 10000 * CAIO.TotalWT)
CAIO['RbN'] = (CAIO.Rb)/( 10000 * CAIO.TotalWT)

CAIO['TotalAlkali'] = CAIO.K2ON + CAIO.NA2ON

CAIO_Original = CAIO.copy()
dataTOTF_Original = dataTOTF.copy()

CAIO=CAIO.ix[:,['SIO2N','TIO2N','AL2O3N','FE2O3TN','CAON','MGON','MNON','NA2ON','K2ON','P2O5N','NbN','ZrN','LaN','CeN','SrN','BaN','RbN','controlcode']]

dataTOTF=dataTOTF.ix[:,['SIO2N','TIO2N','AL2O3N','FE2O3TN','CAON','MGON','MNON','NA2ON','K2ON','P2O5N','NbN','ZrN','LaN','CeN','SrN','BaN','RbN','controlcode']]

frames = [dataTOTF, CAIO]
    
# put the unknownks and the GEOROK data in the sam dataset
dataTOTF = pd.concat(frames,ignore_index=True)





