# -*- coding: utf-8 -*-

import numpy as np
from scipy import stats
from sklearn.preprocessing import StandardScaler
from skbio.stats.composition import ilr

# data tranformation: TRANSFORM (0 NONE; 1 LOG; 2 BOX-COX; 3 ISOMETRIC-LOG-RATIO)
TRANSFORM = 2



#Do not perform any tranformation
if TRANSFORM == 0:
    dataTOTF1 = pd.DataFrame(data=dataTOTF.values, columns=dataTOTF.columns,index=dataTOTF.index)
    # define the  dataset (U17Dmt) with the relative labels (y17Dmt)
    U17Dmt = dataTOTF1[(dataTOTF1.controlcode >= 0)][['SIO2N','TIO2N','AL2O3N','FE2O3TN','CAON','MGON','MNON','NA2ON','K2ON','P2O5N','NbN','ZrN','LaN','CeN','SrN','BaN','RbN']].values
    y17Dmt =  dataTOTF1[(dataTOTF1.controlcode >= 0)][['controlcode']].values

    
#Performs log tranformation
if TRANSFORM == 1:
    dataTOTF1 = pd.DataFrame(data=dataTOTF.values, columns=dataTOTF.columns,index=dataTOTF.index)
    aa = range(0,16)
    for count in aa:
        aa1 = dataTOTF.iloc[:,count]
        dataTOTF1[dataTOTF1.columns[count]]= np.log10(aa1)
        # define the  dataset (U17Dmt) with the relative labels (y17Dmt)
        U17Dmt = dataTOTF1[(dataTOTF1.controlcode >= 0)][['SIO2N','TIO2N','AL2O3N','FE2O3TN','CAON','MGON','MNON','NA2ON','K2ON','P2O5N','NbN','ZrN','LaN','CeN','SrN','BaN','RbN']].values
        y17Dmt =  dataTOTF1[(dataTOTF1.controlcode >= 0)][['controlcode']].values

    
#Performs Box-Cox tranformation
if TRANSFORM == 2:
    dataTOTF1 = pd.DataFrame(data=dataTOTF.values, columns=dataTOTF.columns,index=dataTOTF.index)
    aa = range(0,16)
    for count in aa:
        aa1 = stats.boxcox(dataTOTF.iloc[:,count])
        dataTOTF1[dataTOTF1.columns[count]]= aa1[0] 
        # define the  dataset (U17Dmt) with the relative labels (y17Dmt)
        U17Dmt = dataTOTF1[(dataTOTF1.controlcode >= 0)][['SIO2N','TIO2N','AL2O3N','FE2O3TN','CAON','MGON','MNON','NA2ON','K2ON','P2O5N','NbN','ZrN','LaN','CeN','SrN','BaN','RbN']].values
        y17Dmt =  dataTOTF1[(dataTOTF1.controlcode >= 0)][['controlcode']].values
    
        
# Performs isometric log-ratio transformation.

if TRANSFORM == 3:
    aa = range(0,len(dataTOTF.index)-1)
    for count in aa:        
        aa1 = ilr(dataTOTF.iloc[count,0:17])
        if count == 0:
            U17Dmt = np.array([aa1])
            y17Dmt=np.array([dataTOTF.iloc[count,17]])
        else:  
            U17Dmt = np.append(U17Dmt,[aa1],axis=0)
            y17Dmt = np.append(y17Dmt,dataTOTF.iloc[count,17])
            
        

# subtract the average and trasform to unit variance (second step of the normalization procedure)
X17Dmt = StandardScaler().fit_transform(U17Dmt)
